package com.example.tp1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ArrayAdapter;
import android.content.Intent;

/*
 *
 * Création de la première vue
 *
 */
public class MainActivity extends AppCompatActivity
{
    /*
    *
    * Création de la première activité
    *
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //On crée un ArrayAdapter qui fera apparaître les Strings dans la ListView
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, AnimalList.getNameArray());

        ListView listview = (ListView) findViewById(R.id.listView);
        listview.setAdapter(adapter);

        //Quand on clique sur un animal, on envoie les données à la vue suivante
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long l)
            {
                String ClickListView = (String) parent.getItemAtPosition(i);
                Intent intent = new Intent(getApplicationContext(), AnimalActivity.class);
                intent.putExtra("Listviewclickvalue", ClickListView);
                startActivity(intent);
            }
        });
    }
}