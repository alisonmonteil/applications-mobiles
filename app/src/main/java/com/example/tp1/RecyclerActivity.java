package com.example.tp1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.DividerItemDecoration;
import android.view.ViewGroup;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;
import android.widget.ImageView;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;

/*
*
* Création d'une vue avec un RecyclerView
*
 */
public class RecyclerActivity extends AppCompatActivity
{
    private static final String[] icon = AnimalList.getNameArray();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        RecyclerView recyclerview = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        recyclerview.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerview.setAdapter(new IconicAdapter());
    }

    //Création d'une classe IconicAdapter qui va permettre de peupler le RecyclerView avec les données pour chaque index grâce au RowHolder
    class IconicAdapter extends RecyclerView.Adapter<RowHolder>
    {
        @NonNull
        @Override
        public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
        {
            return(new RowHolder(getLayoutInflater().inflate(R.layout.row, parent, false)));
        }

        @Override
        public void onBindViewHolder(@NonNull RowHolder holder, int position)
        {
            holder.bindModel(icon[position]);
        }

        @Override
        public int getItemCount()
        {
            return(icon.length);
        }
    }

    //Création de la classe RowHolder
    class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        TextView label = null;
        ImageView icon = null;

        public RowHolder(View row)
        {
            super(row);
            row.setOnClickListener(this);
            label = (TextView) row.findViewById(R.id.textView180);
            icon = (ImageView) row.findViewById(R.id.imageView180);
        }

        @Override
        public void onClick(View v)
        {
            String animalText = label.getText().toString();
            Intent intent = new Intent(RecyclerActivity.this, AnimalActivity.class);
            intent.putExtra("Listviewclickvalue", animalText);
            startActivity(intent);
        }

        void bindModel(String item)
        {
            label.setText(item);
            Animal animal = AnimalList.getAnimal(item);
            Resources resource = getResources();
            int id = resource.getIdentifier(animal.getImgFile(), "drawable", getPackageName());
            Drawable drawable = resource.getDrawable(id);
            icon.setImageDrawable(drawable);
        }
    }
}
