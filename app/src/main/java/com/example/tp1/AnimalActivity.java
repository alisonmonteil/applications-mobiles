package com.example.tp1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.widget.TextView;
import android.widget.ImageView;
import android.widget.EditText;
import android.widget.Button;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.View;

/*
*
* Création de la seconde vue
*
 */
public class AnimalActivity extends AppCompatActivity
{
    /*
    *
    * Création de la deuxième activité
    *
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);

        //Permet de récupérer la valeur que l'on a envoyé
        Intent intent = getIntent();
        String listviewclick = intent.getStringExtra("Listviewclickvalue");
        final Animal animal = AnimalList.getAnimal(listviewclick);

        //Récupère l'image de l'animal
        ImageView image = findViewById(R.id.imageView2);
        Resources resource = getResources();
        int id = resource.getIdentifier(animal.getImgFile(),"drawable", getPackageName());
        Drawable drawable = resource.getDrawable(id);
        image.setImageDrawable(drawable);

        //Permet de remplir les TextView et EditView en récupérant les données dans la hashmap
        TextView name = findViewById(R.id.textView7);
        name.setText(listviewclick);

        TextView hope = findViewById(R.id.textView9);
        hope.setText(animal.getStrHightestLifespan());

        TextView gestation = findViewById(R.id.textView11);
        gestation.setText(animal.getStrGestationPeriod());

        TextView birthWeight = findViewById(R.id.textView13);
        birthWeight.setText(animal.getStrBirthWeight());

        TextView adultWeight = findViewById(R.id.textView15);
        adultWeight.setText(animal.getStrAdultWeight());

        final EditText retention = findViewById(R.id.editText7);
        retention.setText(animal.getConservationStatus());

        Button button = findViewById(R.id.button);

        //Permet de sauvegarder le EditView lorsque l'on veut le modifier
        button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String change = retention.getText().toString();
                animal.setConservationStatus(change);
                finish();
            }
        });
    }
}